package com.dicoding.widiarta.submission3new.Interface;

import com.dicoding.widiarta.submission3new.Model.Genre;
import java.util.List;

public interface OnGetGenresCallback {
    void onSuccess(List<Genre> genres);

    void onError();
}
