package com.dicoding.widiarta.submission3new.Interface;

import com.dicoding.widiarta.submission3new.Model.Movie;
import java.util.List;

public interface OnGetMoviesCallback {
    void onSuccess(List<Movie> movies);

    void onError();
}
